package niyuby.app.com.eatitkotlin.activities

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.paypal.android.sdk.payments.*
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.database.Database
import niyuby.app.com.eatitkotlin.helper.RecyclerItemTouchHelper
import niyuby.app.com.eatitkotlin.interfacemodel.RecyclerItemTouchHelperListener
import niyuby.app.com.eatitkotlin.model.Order
import niyuby.app.com.eatitkotlin.model.Request
import niyuby.app.com.eatitkotlin.viewholder.CartAdapter
import niyuby.app.com.eatitkotlin.viewholder.CartViewHolder
import java.text.NumberFormat
import java.util.*

class CartActivity : AppCompatActivity(), RecyclerItemTouchHelperListener {
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var databaseReference: DatabaseReference //request
    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var txt_price: TextView
    lateinit var btn_placeOrder: Button
     var cart: List<Order> = ArrayList()
    lateinit var adapter: CartAdapter
    lateinit var rooot_layout: RelativeLayout
    lateinit var address: String
    lateinit var comment: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        //Empezando con Firebase
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Requests")

        //Cargando el menu
        recyclerView = findViewById<View>(R.id.recycler_cart) as RecyclerView
        recyclerView.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this@CartActivity)
        recyclerView.layoutManager = layoutManager

        //Borrando las ordenes
        val simpleCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this@CartActivity)
        ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerView)


        txt_price = findViewById<View>(R.id.txt_total) as TextView
        btn_placeOrder = findViewById<View>(R.id.btn_place_order) as Button
        btn_placeOrder.setOnClickListener { showAlertDialog() }
        rooot_layout = findViewById<View>(R.id.layout_root) as RelativeLayout



        loadListFood()
    }

    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this@CartActivity)
        alertDialog.setTitle("One more step!").setMessage("Enter your address: ")

        val edtxtAddress = EditText(this@CartActivity)
        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)

        edtxtAddress.layoutParams = layoutParams
        alertDialog.setView(edtxtAddress)
        alertDialog.setIcon(R.drawable.ic_shopping_cart_black_24dp)

        alertDialog.setPositiveButton("YES") { dialogInterface, i ->
            val request = Request(
                    Common.commonUser.phone,
                    Common.commonUser.name,
                    edtxtAddress.text.toString(),
                    txt_price.text.toString(),
                    cart
            )
            // Actualizando para Firebase
            databaseReference.child(System.currentTimeMillis().toString()).setValue(request)
            Database(baseContext).cleanCart()
            Toast.makeText(this@CartActivity, "Thank you, order place", Toast.LENGTH_LONG).show()
            finish()
        }
        alertDialog.setNegativeButton("NO") { dialogInterface, i -> dialogInterface.dismiss() }
        alertDialog.show()
    }

    private fun loadListFood() {
        cart = Database(this).getCarts(Common.commonUser.phone!!)
        adapter = CartAdapter(cart as MutableList<Order>, this)
        adapter.notifyDataSetChanged()
        recyclerView.adapter = adapter

        // Calculando el precio total

        var total = 0

        for (order in cart)
            total += Integer.parseInt(order.price) * Integer.parseInt(order.quantity)
        val locale = Locale("en", "US")
        val numberFormat = NumberFormat.getCurrencyInstance(locale)
        txt_price.text = numberFormat.format(total.toLong())
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is CartViewHolder) {
            val name = (recyclerView.adapter as CartAdapter).getItem(viewHolder.getAdapterPosition()).productName
            val deteleItem = (recyclerView.adapter as CartAdapter).getItem(viewHolder.getAdapterPosition())
            val deleteIndex = viewHolder.getAdapterPosition()

            adapter.removeItem(deleteIndex)
            Database(baseContext).removeFromCart(deteleItem.productId)
            var total = 0
            val orders = Database(baseContext).getCarts(Common.commonUser.phone!!)
            for (item in orders)
                total += Integer.parseInt(item.price) * Integer.parseInt(item.quantity)

            val locale = Locale("en", "US")
            val numberFormat = NumberFormat.getCurrencyInstance(locale)
            txt_price.text = numberFormat.format(total.toLong())

            //snackBar
            val snackbar = Snackbar.make(rooot_layout, "$name removed from cart!", Snackbar.LENGTH_LONG)
            snackbar.setAction("UNDO") {
                adapter.restoreItem(deteleItem, deleteIndex)
                Database(baseContext).addToCart(deteleItem)
                var total = 0

                for (order in cart)
                    total += Integer.parseInt(order.price) * Integer.parseInt(order.quantity)
                val locale = Locale("en", "US")
                val numberFormat = NumberFormat.getCurrencyInstance(locale)
                txt_price.text = numberFormat.format(total.toLong())
            }
            snackbar.setActionTextColor(Color.GREEN)
            snackbar.show()
        }
    }
}
