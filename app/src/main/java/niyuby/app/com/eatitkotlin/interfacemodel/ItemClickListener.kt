package niyuby.app.com.eatitkotlin.interfacemodel

import android.view.View

interface ItemClickListener {
    fun onClick(view: View, position: Int, isLongClick: Boolean)
}
