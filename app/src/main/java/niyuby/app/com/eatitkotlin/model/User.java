package niyuby.app.com.eatitkotlin.model;
public class User {
    private String Name;

    private String Password;

    private String Phone;

    private String secureCode;

    public String getSecureCode() {
        return secureCode;
    }

    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }

    public User() {
    }

    public User(String name, String password, String phone,String secureCode) {
        Name = name;
        Password = password;
        Phone=phone;
        this.secureCode = secureCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    @Override
    public String toString(){
        return
                "JsonMember0988123344{" +
                        "name = '" + Name + '\'' +
                        ",password = '" + Password + '\'' +
                        ",phone = '" + Phone + '\'' +
                        ",secureCode = '" + secureCode + '\'' +
                        "}";
    }
}