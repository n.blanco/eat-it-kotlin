package niyuby.app.com.eatitkotlin.viewholder

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.text.NumberFormat
import java.util.ArrayList
import java.util.Locale

import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.activities.CartActivity
import niyuby.app.com.eatitkotlin.model.Order

class CartAdapter(listData: MutableList<Order>, context: CartActivity) : RecyclerView.Adapter<CartViewHolder>() {
    private var listData = ArrayList<Order>()
    private val context: Context
    private val cart: CartActivity

    init {
        this.listData = listData as ArrayList<Order>
        this.context = context
        this.cart = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val inflater = LayoutInflater.from(cart)
        val view = inflater.inflate(R.layout.cart_layout, parent, false)
        return CartViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val locale = Locale("en", "US")
        val numberFormat = NumberFormat.getCurrencyInstance(locale)
        val price = Integer.parseInt(listData[position].price) * Integer.parseInt(listData[position].quantity)
        /*Order order=listData.get(position);
        new Database(cart).updateCart(order);
        int total=0;
        List<Order> orders=new Database(cart).getCarts(Common.commonUser.getPhone());
        for(Order item:orders)

            total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(item.getQuantity()));
        Locale locale=new Locale("en","US");
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);*/

        //listData=new Database(cart).getCarts(Common.commonUser.getPhone());

        //Calculando el precio total

        holder.txt_price.text = numberFormat.format(price.toLong())

        holder.txt_cart_name.text = listData[position].productName

    }

    override fun getItemCount(): Int {
        return listData.size
    }

    fun getItem(position: Int): Order {
        return listData[position]
    }

    fun removeItem(position: Int) {
        listData.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: Order, position: Int) {
        listData.add(position, item)
        notifyItemInserted(position)
    }
}

