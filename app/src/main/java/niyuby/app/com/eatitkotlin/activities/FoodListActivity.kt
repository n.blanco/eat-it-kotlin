package niyuby.app.com.eatitkotlin.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.database.Database
import niyuby.app.com.eatitkotlin.interfacemodel.ItemClickListener
import niyuby.app.com.eatitkotlin.model.Foods
import niyuby.app.com.eatitkotlin.viewholder.FoodsViewHolder

class FoodListActivity : AppCompatActivity() {
    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var swipe: SwipeRefreshLayout
    lateinit var databaseReference: DatabaseReference
     var categoryId: String? = ""
    lateinit var localDB: Database
    lateinit var adapter: FirebaseRecyclerAdapter<Foods, FoodsViewHolder>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_list)
        //Empezando con Firebase
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Foods")
        localDB = Database(this)
        //Cargando el menu
        recyclerView = findViewById<View>(R.id.recycler_list) as RecyclerView
        recyclerView.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this@FoodListActivity)
        recyclerView.layoutManager = layoutManager
        swipe = findViewById<View>(R.id.swipe_list) as SwipeRefreshLayout
        swipe.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent)
        swipe.setOnRefreshListener {
            // Esto se ejecuta cada vez que se realiza el gesto
            if (intent != null) {
                categoryId = intent.getStringExtra("CategoryId")
                if (!categoryId!!.isEmpty() && categoryId != null) {
                    if (Common.isConnectedToInternet(baseContext)) {
                        loadListFood(categoryId!!)
                    }else {
                        Toast.makeText(this@FoodListActivity, "Please check your connection", Toast.LENGTH_LONG).show()
                        return@setOnRefreshListener
                    }
                }

            }
        }
        swipe.post {  if (intent != null) {
            categoryId = intent.getStringExtra("CategoryId")
            if (!categoryId!!.isEmpty() && categoryId != null) {
                loadListFood(categoryId!!)
            }

        }}
        //Obteniendo el intent
        if (intent != null) {
            categoryId = intent.getStringExtra("CategoryId")
            if (!categoryId!!.isEmpty() && categoryId != null) {
                loadListFood(categoryId!!)
            }

        }
    }

    private fun loadListFood(categoryId: String) {
        adapter = object : FirebaseRecyclerAdapter<Foods, FoodsViewHolder>(Foods::class.java!!,
                R.layout.food_item, FoodsViewHolder::class.java,
                databaseReference.orderByChild("menuId").equalTo(categoryId)) {
            override fun populateViewHolder(viewHolder: FoodsViewHolder, model: Foods, position: Int) {
                viewHolder.textView.text = model.name
                Picasso.with(baseContext).load(model.image).error(R.drawable.error).into(viewHolder.imageView)
                if (localDB.isFavorite(adapter.getRef(position).key)) {
                    viewHolder.imageFav.setImageResource(R.drawable.ic_favorite_black_24dp)
                }
                viewHolder.imageFav.setOnClickListener {
                    if (!localDB.isFavorite(adapter.getRef(position).key)) {
                        localDB.addtoFavorites(adapter.getRef(position).key)
                        viewHolder.imageFav.setImageResource(R.drawable.ic_favorite_black_24dp)
                        Toast.makeText(this@FoodListActivity, "" + model.name + " was added to Favorites", Toast.LENGTH_LONG).show()
                    } else {
                        localDB.removeFromFavorites(adapter.getRef(position).key)
                        viewHolder.imageFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)
                        Toast.makeText(this@FoodListActivity, "" + model.name + " was removed from Favorites", Toast.LENGTH_LONG).show()

                    }
                }
                val foods = model
                viewHolder.setItemClickListener(object : ItemClickListener {
                    override fun onClick(view: View, position: Int, isLongClick: Boolean) {
                       // Toast.makeText(this@FoodListActivity,""+foods.getName(), Toast.LENGTH_LONG).show();
                        //Toast.makeText(this@FoodListActivity,""+foods.getImage(), Toast.LENGTH_LONG).show();

                        val foodDetail = Intent(this@FoodListActivity, FoodDetailActivity::class.java)
                        foodDetail.putExtra("FoodId", adapter.getRef(position).key)
                        startActivity(foodDetail)
                    }
                })
            }
        }
        recyclerView.adapter = adapter
        swipe.isRefreshing = false
    }
}
