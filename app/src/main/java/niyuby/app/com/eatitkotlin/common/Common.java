package niyuby.app.com.eatitkotlin.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.ContextThemeWrapper;

import niyuby.app.com.eatitkotlin.model.User;

public class Common {
    public static User commonUser;

    public static boolean  isConnectedToInternet(Context context){
        ConnectivityManager connectivityManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager!=null){
            NetworkInfo[] infos=connectivityManager.getAllNetworkInfo();
            if(infos!=null){
                for(int i=0;i<infos.length;i++){
                    if (infos[i].getState()==NetworkInfo.State.CONNECTED)
                        return true;
                }
            }
        }
        return false;
    }
}

