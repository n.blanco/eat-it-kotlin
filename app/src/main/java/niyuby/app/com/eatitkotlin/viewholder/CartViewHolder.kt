package niyuby.app.com.eatitkotlin.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.interfacemodel.ItemClickListener

class CartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    var txt_cart_name: TextView
    var txt_price: TextView
    var img_count: ImageView
    private val itemClickListener: ItemClickListener? = null

    var view_background: RelativeLayout
    var view_foreground: LinearLayout

    init {
        txt_cart_name = itemView.findViewById<View>(R.id.txt_cart_item_name) as TextView
        txt_price = itemView.findViewById<View>(R.id.txt_cart_item_price) as TextView
        img_count = itemView.findViewById<View>(R.id.img_cart_item_count) as ImageView
        view_background = itemView.findViewById<View>(R.id.view_background) as RelativeLayout
        view_foreground = itemView.findViewById<View>(R.id.view_forewround) as LinearLayout
    }

    override fun onClick(view: View) {

    }
}