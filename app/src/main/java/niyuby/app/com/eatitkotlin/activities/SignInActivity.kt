package niyuby.app.com.eatitkotlin.activities

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.google.firebase.database.*
import com.rengwuxian.materialedittext.MaterialEditText
import kotlinx.android.synthetic.main.activity_sign_in.*
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.model.User
import android.view.View
class SignInActivity : AppCompatActivity() {
     lateinit var firebaseDatabase: FirebaseDatabase
     lateinit var databaseReference: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in) //Comenzando con Firebase
        txt_forgot_pass.setOnClickListener { showDialogForgot() }
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("User")
        btn_signIn.setOnClickListener {

            if (Common.isConnectedToInternet(baseContext)) {
                val progressDialog = ProgressDialog(this@SignInActivity)
                progressDialog.setMessage("Please waiting...")
                progressDialog.show()
                databaseReference.addListenerForSingleValueEvent(object : ValueEventListener {
                    // cambiando el metodo
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        // Verficando que existe el usuario...
                        if (dataSnapshot.child(edittxt_phone.text.toString()).exists()) {
                            //Obteniendo la informacion...
                            progressDialog.dismiss()
                            val user = dataSnapshot.child(edittxt_phone.text.toString()).getValue<User>(User::class.java)
                            user!!.phone = edittxt_phone.text.toString()

                            if (user!!.password == edittxt_pass.text.toString()) {
                                //Toast.makeText(SignInActivity.this,"Sign In is successfully!",Toast.LENGTH_LONG).show();
                                val homeScreen = Intent(this@SignInActivity, MenuFoodsActivity::class.java)
                                Common.commonUser = user
                                startActivity(homeScreen)
                                finish()
                                databaseReference.removeEventListener(this)

                            } else {
                                Toast.makeText(this@SignInActivity, "Wrong password", Toast.LENGTH_LONG).show()
                            }
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(this@SignInActivity, "User not exist", Toast.LENGTH_LONG).show()

                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                })
            }
                else {
                Toast.makeText(this@SignInActivity, "Please check your connection", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
        }
    }

    private fun showDialogForgot() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Forgot password").setMessage("Enter your secure code: ")

        val inflater = this.layoutInflater
        val forgot_view = inflater.inflate(R.layout.forgot_pass_layout, null)
        alertDialog.setView(forgot_view)

        val editText2 = forgot_view.findViewById<View>(R.id.edittxt_phone) as MaterialEditText
        val editText3 = forgot_view.findViewById<View>(R.id.edittxt_secure_code) as MaterialEditText

        alertDialog.setPositiveButton("YES") { dialogInterface, i ->
            databaseReference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val user = dataSnapshot.child(editText2.text.toString()).getValue<User>(User::class.java)
                    if (user!!.secureCode == editText3.text.toString())
                        Toast.makeText(this@SignInActivity, "Your Password" + user!!.password!!, Toast.LENGTH_LONG).show()
                    else
                        Toast.makeText(this@SignInActivity, "Wrong secure code!", Toast.LENGTH_LONG).show()

                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }
        alertDialog.setNegativeButton("NO") { dialogInterface, i -> dialogInterface.dismiss() }
        alertDialog.show()
    }
}
