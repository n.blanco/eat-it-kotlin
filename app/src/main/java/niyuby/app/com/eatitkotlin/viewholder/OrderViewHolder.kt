package niyuby.app.com.eatitkotlin.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import niyuby.app.com.eatitkotlin.R

import niyuby.app.com.eatitkotlin.interfacemodel.ItemClickListener

class OrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

     var txtOrderId: TextView
     var txtOrderStatus: TextView
     var txtOrderPhone: TextView
     var txtOrderAddress: TextView
    private var itemClickListener: ItemClickListener? = null


    init{
        txtOrderId = itemView.findViewById<View>(R.id.txt_order_name) as TextView
        txtOrderStatus = itemView.findViewById<View>(R.id.txt_status) as TextView
        txtOrderPhone = itemView.findViewById<View>(R.id.txt_order_phone) as TextView
        txtOrderAddress = itemView.findViewById<View>(R.id.txt_order_address) as TextView

    }

}
