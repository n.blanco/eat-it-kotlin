package niyuby.app.com.eatitkotlin.database

import android.content.Context
import android.database.sqlite.SQLiteQueryBuilder
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper
import niyuby.app.com.eatitkotlin.model.Order
import java.util.ArrayList

class Database(context: Context) : SQLiteAssetHelper(context, DB_NAME, null, DB_VER) {

    fun getCarts(s: String): List<Order> {
        val database = readableDatabase
        val queryBuilder = SQLiteQueryBuilder()

        val sqlSelect = arrayOf("ID", "ProductName", "ProductId", "Quantity", "Price", "Discount")
        val sqlTable = "OrderDetail"

        queryBuilder.tables = sqlTable
        val c = queryBuilder.query(database, sqlSelect, null, null, null, null, null)

        val result = ArrayList<Order>()

        if (c.moveToFirst()) {
            do {
                result.add(Order(
                        c.getInt(c.getColumnIndex("ID")),
                        c.getString(c.getColumnIndex("ProductId")),
                        c.getString(c.getColumnIndex("ProductName")),
                        c.getString(c.getColumnIndex("Quantity")),
                        c.getString(c.getColumnIndex("Price")),
                        c.getString(c.getColumnIndex("Discount"))))

            } while (c.moveToNext())
        }
        return result
    }

    fun addToCart(order: Order) {
        val database = readableDatabase
        val query = String.format("INSERT INTO OrderDetail(ProductId,ProductName,Quantity,Price,Discount) VALUES('%s','%s','%s','%s','%s');",
                order.productId,
                order.productName,
                order.quantity,
                order.price,
                order.discount)
        database.execSQL(query)

    }

    fun updateCart(order: Order) {
        val database = readableDatabase
        val query = String.format("UPDATE OrderDetail SET Quantity= %s WHERE ID = %d",
                order.quantity,
                order.id)
        database.execSQL(query)

    }

    fun removeFromCart(productId: String) {
        val database = readableDatabase
        val query = String.format("DELETE FROM OrderDetail WHERE ProductId='%s'", productId)

        database.execSQL(query)
    }

    fun cleanCart() {
        val database = readableDatabase
        val query = String.format("DELETE FROM OrderDetail")

        database.execSQL(query)

    }

    fun removeFromFavorites(foodId: String){
        val database = readableDatabase
        val query = String.format("DELETE FROM Favorites WHERE FoodId='%s';",foodId)

        database.execSQL(query)
    }
   fun addtoFavorites(foodId:String){
       val database = readableDatabase
       val query = String.format("INSERT INTO Favorites(FoodId) VALUES('%s');",foodId)
       database.execSQL(query)
   }
    fun isFavorite(foodId: String): Boolean {
        val database = readableDatabase
        val query = String.format("SELECT * FROM Favorites WHERE FoodId='%s';", foodId)
        val cursor = database.rawQuery(query, null)
        if (cursor.count <= 0) {
            cursor.close()
            return false
        }
        cursor.close()
        return true


    }
    companion object {
        private val DB_NAME = "EatItDB.db"
        private val DB_VER = 1
    }


}