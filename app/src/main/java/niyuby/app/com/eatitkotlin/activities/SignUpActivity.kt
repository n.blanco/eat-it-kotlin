package niyuby.app.com.eatitkotlin.activities

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_sign_up.*
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.model.User

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        //Comenzando con Firebase

        val firebaseDatabase = FirebaseDatabase.getInstance()
        val databaseReference = firebaseDatabase.getReference("User")
        btn_signUp.setOnClickListener {
            if (Common.isConnectedToInternet(baseContext)) {
                val progressDialog = ProgressDialog(this@SignUpActivity)
                progressDialog.setMessage("Please waiting...")
                progressDialog.show()
                databaseReference.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        // Verficando que existe el usuario...
                        if (dataSnapshot.child(edittxt_phone.text.toString()).exists()) {
                            //Obteniendo la informacion...
                            progressDialog.dismiss()
                            Toast.makeText(this@SignUpActivity, "Phone number already register", Toast.LENGTH_LONG).show()


                        } else {
                            progressDialog.dismiss()
                            val user = User(edittxt_phone.text.toString(), edittxt_name.text.toString(), edittxt_pass.text.toString(),
                                    edittxt_secure_code.text.toString())
                            databaseReference.child(edittxt_phone.text.toString()).setValue(user)
                            Toast.makeText(this@SignUpActivity, "Sign up successfully", Toast.LENGTH_LONG).show()
                            finish()
                        }

                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                })
            } else {
                Toast.makeText(this@SignUpActivity, "Please check your connection", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
        }
    }
}
