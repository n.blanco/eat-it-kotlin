package niyuby.app.com.eatitkotlin.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.database.Database
import niyuby.app.com.eatitkotlin.model.Foods
import niyuby.app.com.eatitkotlin.model.Order

class FoodDetailActivity : AppCompatActivity() {
    lateinit var foodName: TextView
    lateinit var foodPrice: TextView
    lateinit var foodDescription: TextView
    lateinit var imgFood: ImageView
    lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    lateinit var actionButton: FloatingActionButton
    lateinit var buttonNumber: ElegantNumberButton

     var foodId: String? = ""
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var foods: DatabaseReference
     var currentFoods: Foods? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_detail)

        //Firebase
        firebaseDatabase = FirebaseDatabase.getInstance()
        foods = firebaseDatabase.getReference("Foods")

        // Vista
        buttonNumber = findViewById<View>(R.id.number_button) as ElegantNumberButton
        actionButton = findViewById<View>(R.id.floating_cart) as FloatingActionButton

        actionButton.setOnClickListener {
            Database(baseContext).addToCart(Order(foodId,
                    currentFoods!!.name,
                    buttonNumber.number,
                    currentFoods!!.price,
                    currentFoods!!.discount


            ))
            Toast.makeText(this@FoodDetailActivity, "Add to Cart", Toast.LENGTH_LONG).show()
        }

        foodName = findViewById<View>(R.id.food_name) as TextView
        foodPrice = findViewById<View>(R.id.food_price) as TextView
        foodDescription = findViewById<View>(R.id.txt_foodDescription) as TextView

        imgFood = findViewById<View>(R.id.img_food) as ImageView

        collapsingToolbarLayout = findViewById<View>(R.id.collapsing_toolbar) as CollapsingToolbarLayout
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)

        //Obteniendo el intent
        if (intent != null) {
            foodId = intent.getStringExtra("FoodId")
            if (!foodId!!.isEmpty() && foodId != null) {
                if (Common.isConnectedToInternet(baseContext)) {
                    loadFood(foodId!!)
                }else {
                    Toast.makeText(this@FoodDetailActivity, "Please check your connection", Toast.LENGTH_LONG).show()
                    return
                }
            }

        }


    }

    private fun loadFood(foodId: String) {
        foods.child(foodId).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                currentFoods = dataSnapshot.getValue<Foods>(Foods::class.java)

                //Imagen
                Picasso.with(baseContext).load(currentFoods!!.image).error(R.drawable.error).into(imgFood)

                collapsingToolbarLayout.title = currentFoods!!.name

                foodPrice.text = currentFoods!!.price
                foodName.text = currentFoods!!.name
                foodDescription.text = currentFoods!!.description
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }
}
