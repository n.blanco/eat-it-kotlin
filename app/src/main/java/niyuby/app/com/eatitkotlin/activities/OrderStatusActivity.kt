package niyuby.app.com.eatitkotlin.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.interfacemodel.ItemClickListener
import niyuby.app.com.eatitkotlin.model.Category
import niyuby.app.com.eatitkotlin.model.Request
import niyuby.app.com.eatitkotlin.viewholder.MenuViewHolder
import niyuby.app.com.eatitkotlin.viewholder.OrderViewHolder

class OrderStatusActivity : AppCompatActivity() {
    lateinit var recyclerView: RecyclerView
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var databaseReference: DatabaseReference
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var adapter: FirebaseRecyclerAdapter<Request, OrderViewHolder>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_status)
        //Empezando con Firebase
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Requests")

        recyclerView = findViewById<View>(R.id.recycler_orders) as RecyclerView
        recyclerView.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this@OrderStatusActivity)
        recyclerView.layoutManager = layoutManager

        loadOrders(Common.commonUser.phone)
    }

    private fun loadOrders(phone: String) {
        adapter = object : FirebaseRecyclerAdapter<Request, OrderViewHolder>(Request::class.java,
                R.layout.order_layout, OrderViewHolder::class.java, databaseReference.orderByChild("phone").equalTo(phone)) {
            override fun populateViewHolder(viewHolder: OrderViewHolder, model: Request, position: Int) {
                viewHolder.txtOrderId.text = adapter.getRef(position).key
                viewHolder.txtOrderStatus.text = convertCodeToStatus(model.status)
                viewHolder.txtOrderAddress.text = model.address
                viewHolder.txtOrderPhone.text = model.phone
            }
        }
        recyclerView.adapter = adapter
    }

    private fun convertCodeToStatus(status: String): String {
        if (status == "0") {
            return "Placed"
        }
        return if (status == "1") {
            "On my way"
        } else
            "Shipped"
    }
}