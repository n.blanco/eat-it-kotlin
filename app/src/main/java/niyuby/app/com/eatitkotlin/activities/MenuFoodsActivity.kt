package niyuby.app.com.eatitkotlin.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.rengwuxian.materialedittext.MaterialEditText
import com.squareup.picasso.Picasso
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_menu_foods.*
import kotlinx.android.synthetic.main.app_bar_menu_foods.*
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.common.Common
import niyuby.app.com.eatitkotlin.interfacemodel.ItemClickListener
import niyuby.app.com.eatitkotlin.model.Category
import niyuby.app.com.eatitkotlin.viewholder.MenuViewHolder
import java.util.HashMap

class MenuFoodsActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var firebaseDatabase: FirebaseDatabase
    lateinit var databaseReference: DatabaseReference
    lateinit var txt_fullname: TextView
    lateinit var recyclerView: RecyclerView
    lateinit var swipe: SwipeRefreshLayout
    lateinit var layoutManager: RecyclerView.LayoutManager
    lateinit var adapter: FirebaseRecyclerAdapter<Category, MenuViewHolder>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_foods)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
//Empezando con Firebase
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference("Category")

        val fab = findViewById<View>(R.id.fab) as FloatingActionButton

        fab.setOnClickListener {
            val cartIntent = Intent(this@MenuFoodsActivity, CartActivity::class.java)
            startActivity(cartIntent)
        }
        swipe = findViewById<View>(R.id.swipe_menu) as SwipeRefreshLayout
        swipe.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent)
        swipe.setOnRefreshListener {
            // Esto se ejecuta cada vez que se realiza el gesto
            loadMenu()
        }
        swipe.post { loadMenu() }
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        // Nombre completo para el usuario
        val headerviw = navigationView.getHeaderView(0)
        txt_fullname = headerviw.findViewById<View>(R.id.txt_fullname) as TextView
        txt_fullname.text = Common.commonUser.name

        //Cargando el menu
        recyclerView = findViewById<View>(R.id.recycler_home) as RecyclerView
        recyclerView.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this@MenuFoodsActivity)
        recyclerView.layoutManager = layoutManager
        if (Common.isConnectedToInternet(baseContext)) {
            loadMenu()
        }else {
            Toast.makeText(this@MenuFoodsActivity, "Please check your connection", Toast.LENGTH_LONG).show()
            return
        }


        nav_view.setNavigationItemSelectedListener(this)
    }
    private fun loadMenu() {
        adapter = object : FirebaseRecyclerAdapter<Category, MenuViewHolder>(Category::class.java,
                R.layout.menu_item, MenuViewHolder::class.java, databaseReference) {
            override fun populateViewHolder(viewHolder: MenuViewHolder, model: Category, position: Int) {
                viewHolder.textView.text = model.name
                Picasso.with(baseContext).load(model.image).error(R.drawable.error).into(viewHolder.imageView)

                val category = model
                viewHolder.setItemClickListener(object : ItemClickListener {
                    override fun onClick(view: View, position: Int, isLongClick: Boolean) {
                       // Toast.makeText(this@MenuFoodsActivity,""+category.getName(), Toast.LENGTH_LONG).show();
                       // Toast.makeText(this@MenuFoodsActivity,""+category.getImage(), Toast.LENGTH_LONG).show();

                        // Obteniendo la comida de acuerdo a la categoria
                        val foods = Intent(this@MenuFoodsActivity, FoodListActivity::class.java)
                        foods.putExtra("CategoryId", adapter.getRef(position).key)
                        startActivity(foods)
                    }
                })
            }
        }
        recyclerView.adapter = adapter
        swipe.isRefreshing = false
    }
    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_foods, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.nav_cart) {
            val cartIntent = Intent(this@MenuFoodsActivity, CartActivity::class.java)
            startActivity(cartIntent)
        }
        else if (id == R.id.nav_cart_status) {
            val cartOrderIntent = Intent(this@MenuFoodsActivity, OrderStatusActivity::class.java)
            startActivity(cartOrderIntent)
        }

        else if (id == R.id.nav_change_pass) {
            showChangePass()

        } else if (id == R.id.nav_logout) {
            val signInIntent = Intent(this@MenuFoodsActivity, HomeActivity::class.java)
            signInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(signInIntent)
        }
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
    private fun showChangePass() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Change Password...").setMessage("Please fill all information")

        val inflater = this.layoutInflater
        val change_view = inflater.inflate(R.layout.change_pass_layout, null)
        alertDialog.setView(change_view)

        val editText2 = change_view.findViewById<View>(R.id.edittxt_pass) as MaterialEditText
        val editText3 = change_view.findViewById<View>(R.id.edittxt_new_pass) as MaterialEditText
        val editText4 = change_view.findViewById<View>(R.id.edittxt_repeat_pass) as MaterialEditText
        alertDialog.setPositiveButton("CHANGE") { dialogInterface, i ->
            val alertDialog1 = SpotsDialog(this@MenuFoodsActivity)
            alertDialog1.show()
            if (editText2.text.toString() == Common.commonUser.password) {
                if (editText3.text.toString() == editText4.text.toString()) {
                    val updatePass = HashMap<String, Any>()
                    updatePass["Password"] = editText3.text.toString()
                    val database = FirebaseDatabase.getInstance().getReference("User")
                    database.child(Common.commonUser.phone!!).updateChildren(updatePass).addOnCompleteListener {
                        alertDialog1.dismiss()

                        Toast.makeText(this@MenuFoodsActivity, "Password was update", Toast.LENGTH_LONG).show()
                        val signInIntent = Intent(this@MenuFoodsActivity, SignInActivity::class.java)
                        signInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(signInIntent)
                    }.addOnFailureListener { e -> Toast.makeText(this@MenuFoodsActivity, e.message, Toast.LENGTH_LONG).show() }


                } else {
                    alertDialog1.dismiss()
                    Toast.makeText(this@MenuFoodsActivity, "New password doesn't match", Toast.LENGTH_LONG).show()

                }

            } else {
                alertDialog1.dismiss()
                Toast.makeText(this@MenuFoodsActivity, "Wrong old password!", Toast.LENGTH_LONG).show()
            }
        }
        alertDialog.setNegativeButton("NO") { dialogInterface, i -> dialogInterface.dismiss() }
        alertDialog.show()
    }
}
