package niyuby.app.com.eatitkotlin.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import niyuby.app.com.eatitkotlin.R
import niyuby.app.com.eatitkotlin.interfacemodel.ItemClickListener

class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    var textView: TextView
    var imageView: ImageView
    private var itemClickListener: ItemClickListener? = null

    init {
        textView = itemView.findViewById<View>(R.id.txt_menu) as TextView
        imageView = itemView.findViewById<View>(R.id.img_menu) as ImageView
        itemView.setOnClickListener(this)
    }

    fun setItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onClick(view: View) {
        itemClickListener!!.onClick(view, adapterPosition, false)
    }
}