package niyuby.app.com.eatitkotlin.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*
import niyuby.app.com.eatitkotlin.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        btn_signup.setOnClickListener {
            val signUp = Intent(this@HomeActivity, SignUpActivity::class.java)
            startActivity(signUp)
        }
        btn_signin.setOnClickListener {
            val signIn = Intent(this@HomeActivity, SignInActivity::class.java)
            startActivity(signIn)
        }
    }
    }

